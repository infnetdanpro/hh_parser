import requests
import re
from bs4 import BeautifulSoup
import phonenumbers

def detect_phone(s):
    try:
        s = phonenumbers.parse(s, None)
        return s
    except:
        pass
        return None

def clear_element(s):
    try:
        s = s.replace('  ', ' ')
    except:
        pass
    return s

def remove_tags(text):
    try:
        TAG_RE = re.compile(r'<[^>]+>')
        return TAG_RE.sub(' ', text)
    except:
        pass
        return text

def get_phone(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'}
    r = requests.get(url, headers=headers, timeout=10)

    soup = BeautifulSoup(r.content, 'lxml')
    #список тегов, где может находится телефон
    tags = ('a', 'div', 'p', 'span', 'b', 'br', 'strong', 'table', 'td', 'ul', 'li')
    
    elements = list()

    #выводим каждый элемент, чистим мусора и тегов, оставляем только элементы.
    for tag in tags:
        el = soup.find_all(tag)
        for e in el:
            l = remove_tags(str(e)).rstrip()
            l = clear_element(l)
            elements.append(l)

    #ищем потенциальные номера телефонов
    potential_phones = list()
    for e in elements:
        get_phones = re.findall(r'(?:\+|\d)[\d\-\(\) ]{9,}\d', str(e))
        #если найден больше 0 элементов в каждом элементе списка
        if len(get_phones) > 0:
            for g in get_phones:
                potential_phones.append(g)

    #удаляем дубли через кортеж
    potential_phones = list(set(potential_phones))

    real_phones = list()
    for p in potential_phones:
        if detect_phone(p) != None:
            real_phones.append(p)
        else:
            pass

    return real_phones


def main():
    website_phones = get_phone('http://tzgsho.ru/')
    print(website_phones)


if __name__ == '__main__':
    main()