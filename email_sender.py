import smtplib
import email.utils
import sendgrid
from sendgrid.helpers.mail import *

def send_email(to, date, data=None, subject='Вакансии', server='smtp.mailtrap.io', port=25, login='6bc09a441065b6', password='78235317d33aaf', tls=False):
    try:
        server = smtplib.SMTP(server, port)
        server.ehlo()
        if tls == True:
            server.starttls()
        else:
            pass
        server.login(login, password)

        message = "\r\n".join([
            "From: {}".format(login),
            "To: {}".format(to),
            "Subject: {}".format(subject),
            "Date: {}".format(date),
            "Content-Type: text/plain; charset=utf-8",
            "",
            str(data)
        ])
        server.sendmail(login, to, message.encode('utf-8').strip())
        server.quit()
        return True
    except:
        pass
        return False

def sendgrid_send(from_email, to_email, subject, full_data, sendgrid_api_key):
    sg = sendgrid.SendGridAPIClient(sendgrid_api_key)
    from_email = Email(from_email)
    to_email = Email(to_email)
    content = Content("text/plain", full_data)
    try:   
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        #print(response.body)
        #print(response.headers)
        return response.status_code
    except:
        pass
        return False
        