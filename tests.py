import pytest
import os
import json
import sqlite3
from python_webservice_hh import get_content
from python_webservice_hh import save_to_db

from python_webservice_hh import get_websites

def test_create_db(tmpdir):
    """Проверка доступности папки на создание объектов базы"""
    db = tmpdir.mkdir('db_temp').join('sqlite-temp-test.db')
    assert len(tmpdir.listdir()) == 1


def test_get_content():
    api_url = 'https://api.hh.ru/vacancies'
    vacancy_name = "специалист по охране труда"
    result_str = get_content(api_url, vacancy_name) #проверка типа ответа
    result_dict = json.loads(result_str) #проверка возможности превратить строку в словарь dict()
    get_items = result_dict['items'] #должен быть списком (для итераций)
    assert isinstance(result_str, str)
    assert isinstance(result_dict, dict)
    assert isinstance(get_items, list)


#фикстура создает временную базу с таблицей items и передает conn, а with - чтобы закрыть.
@pytest.fixture
def conn(tmpdir):
    db_name = 'sqlite3_db_temp.db'
    db = tmpdir.mkdir('test').join(db_name)
    with sqlite3.connect('{}'.format(db)) as conn:
        cur = conn.cursor()
        cur.execute("""CREATE TABLE items (id INTEGER PRIMARY KEY, item_id text, premium boolean, name text, department text, hast_test boolean, reponse_letter_required boolean, response_url text, sort_point_distance text, published_at text, created_at text, archived boolean, alternate_url text, apply_alternate_url text, insider_interview text, url text, address_city text, address_street text, address_building text, address_description text, address_lat text, address_lng text, address_raw text, address_metro text, address_id text, employer_id text, employer_name text, employer_url text, employer_alternate_url text, employer_logo_url text, employer_vacancies_url text, employer_trusted boolean, snippet_requirement text, snippet_responsibility text, contacts_name text, contacts_email text, contacts_phones text, contacts_phones_comment text, sended_to_email boolean);""")
        cur.execute("""CREATE TABLE items_website (id INTEGER PRIMARY KEY, item_id text, website text UNIQUE, parsed boolean);""")
        conn.commit()
        yield conn

#пробуем записать в таблицу, проверяем на типовых данных (если не запишется на этих данных, значит что-то не так).
#Сюда возможно стоит добавить такие запросы, которые могут ломать что-то в базе.
def test_save_to_db(conn):    
    statement = """INSERT INTO items (item_id, premium, name, department, hast_test, reponse_letter_required, response_url, sort_point_distance, published_at, created_at, archived, insider_interview, apply_alternate_url, address_city, address_street, address_building, address_description, address_lat, address_lng, address_raw, address_metro, address_id, employer_id, employer_name, employer_url, employer_alternate_url, employer_vacancies_url, employer_trusted, snippet_requirement, snippet_responsibility, contacts_name, contacts_email, contacts_phones, contacts_phones_comment, sended_to_email, alternate_url) VALUES ("29739242", "False", "Специалист по охране труда (Мурманск)", "None", "False", "False", "None", "None", "2019-01-18 18:41:32", "2019-01-18 18:41:32", "False", "None", "https://hh.ru/applicant/vacancy_response?vacancyId=29739242", "None", "None", "None", "None", "None", "None", "None", "None", "None", "681883", "Велесстрой", "https://api.hh.ru/employers/681883", "https://hh.ru/employer/681883", "https://api.hh.ru/vacancies?employer_id=681883", "True", "Опыт работы <highlighttext>инженером</highlighttext> <highlighttext>по</highlighttext> <highlighttext>охране</highlighttext> <highlighttext>труда</highlighttext> в сфере строительства от 2-х лет. Опыт разработки, согласования нормативной документации по охране...", "Проведение вводного инструктажа по ТБ;. Подготовка методической документации по охране труда. Ведение и учет документации по охране труда. Ведение отчетности. ", "Артамонов Дмитрий", "artamonovda@velesstroy.com", "7-925-8978352", "None", "False", "https://hh.ru/vacancy/29739242");"""
    try:
        conn.cursor().execute(statement)
        conn.commit()
        result = True
    except:
        result = False

    assert result == True


#проверяем, что правильные запросы не ложат базу и наоборот, если запросы кривые - возникает соотвествующая ошибка. 
#в соответстующий стейтмент заносятся неверные запросы, которые должны вызывать исключения.
def test_get_website_valid_data(conn):
    statements = ["""INSERT INTO items_website (item_id, website, parsed) VALUES ("1231231", "http://yandex.ru/1", "False")""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312314", "http://yandex.ru/2", "False");""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312317", "http://yandex.ru/4", 1)"""]
    for s in statements:
        conn.cursor().execute(s)
        conn.commit()


    bad_statements = ["""INSERT INTO items_website (item_id, website, parsed) VALUES ("1231231", "http://yandex.ru/", "False")""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312314", "http://yandex.ru/2", "False");""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312315", "http://yandex.ru/", False)""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312317", "http://yandex.ru/", 1)""",
    """INSERT INTO items_website (item_id, website, parsed) VALUES ("12312317", "http://yandex.ru/", True)"""]
    for b in bad_statements:
        try:
            conn.cursor().execute(b)    
            conn.commit()
            result_bad_statements = False
            return result_bad_statements
        except sqlite3.IntegrityError:
            result_bad_statements = True
            return result_bad_statements

    assert result_bad_statements == True







def test_get_websites():
    pass