#chcp 65001 for cmd in windows
import requests
import json
import sqlite3
import os.path
from datetime import datetime, timedelta
import email.utils
from email_sender import sendgrid_send
from time import sleep
from phone_parser import get_phone
import sys
import argparse


def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument ('query', nargs='?')

    return parser

###Для пагинации, генерирует контент
def get_content(api_url, search, page=None):
    if page is None:
        url = '{}?text={}'.format(api_url, search)
    else:
        url = '{}?text={}&page={}'.format(api_url, search, page)

    page = requests.get(url)
    return page.content.decode('utf-8')

#превращает контент страницы в словарь для работы
def parse_content(page_content):
    dict_content = json.loads(page_content)
    return dict_content

#функция замены на пробелы тех элементов, которые в базе записаны как None
def none_replacer(s):
    if s == 'None':
        return ''
    else:
        return s

#Функция отправки email, все настройки в email_sender.py, на вход строка
def send(data, email_to='dan-pro352@ya.ru'):
    dt = datetime.now()
    header_datetime = email.utils.format_datetime(dt)
    send_please = send_email(email_to, header_datetime, data)

    return send_please

#Функция замены в параметре ключа дат двух элементов, которые мешали преобразовать строку в datetime, понятный python
def clear_time(s):
    try:
        s = s.replace("T", " ")
        s = s.replace("+0300", "")
        return s
    except:
        pass
        return None

#Функция проверяет существование файла базы sqlite3, если его нет - то создает, если есть - пропускает действие.
#Возвращает True, если создана, False, если не существует ранее и не создана (например, если модуль не импортирован)
#В остальных случаях (и при других ошибках выводит, что база уже существует)
def create_db(db_name):
    file_exist = os.path.isfile(db_name)
    if file_exist == False:
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        try:
            cur.execute("""CREATE TABLE items_website (id INTEGER PRIMARY KEY, item_id text, website text UNIQUE, parsed boolean);""")
            cur.execute("""CREATE TABLE items (id INTEGER PRIMARY KEY, item_id text UNIQUE, premium boolean, name text, department text, hast_test boolean, reponse_letter_required boolean, response_url text, sort_point_distance text, published_at text, created_at text, archived boolean, alternate_url text, apply_alternate_url text, insider_interview text, url text, address_city text, address_street text, address_building text, address_description text, address_lat text, address_lng text, address_raw text, address_metro text, address_id text, employer_id text, employer_name text, employer_url text, employer_alternate_url text, employer_logo_url text, employer_vacancies_url text, employer_trusted boolean, snippet_requirement text, snippet_responsibility text, contacts_name text, contacts_email text, contacts_phones text, contacts_phones_comment text, sended_to_email boolean, last_use text);""")
            conn.commit()
            return True
        except:
            pass
            return False
        cur.close()
        conn.close()
    else:
        pass
        return 'File `{}` already exist!'.format(db_name)

#Функция парсинга и сохранения в базу sqlite, на вход словарь, имя базы и номер страницы (для логирования)
def save_to_db(data, db_name, page):
    try:
        conn = sqlite3.connect(db_name)
        cur = conn.cursor()
        print('Success connect to DB!')
    except:
        pass

    for item in data:
        item_id = item['id']
        premium = item['premium']
        name = item['name']

        hast_test = item['has_test']
        reponse_letter_required = item['response_letter_required']
        response_url = item['response_url']
        sort_point_distance = item['sort_point_distance']

        published_at = clear_time(item['published_at'])
        created_at = clear_time(item['created_at'])

        archived = item['archived']
        alternate_url = item['alternate_url']
        apply_alternate_url = item['apply_alternate_url']
        insider_interview = item['insider_interview']
        
        try:
            department = item['department']['name']
        except:
            department = None

        try:
            employer_name = item['employer']['name']
        except:
            pass
            employer_name = None

        try:
            employer_alternate_url = item['employer']['alternate_url']
        except:
            pass
            employer_alternate_url = None

        try:
            employer_vacancies_url = item['employer']['vacancies_url']
        except:
            pass
            employer_vacancies_url = None

        #Секция адреса (иногда отсутствует)
        #Иногда внутри адреса только улица и номер дома, т.е. данные не всегда полные.

        address_street = None
        address_building = None
        address_description = None
        address_lat = None
        address_lng = None
        address_raw = None
        address_metro = None
        address_id = None
        try:
            address_city = item['address']['city']

            #В случае, если есть адрес, то остальные переменые не факт, что есть.
            try:
                address_street = item['address']['street']
            except:
                pass

            try:
                address_building = item['address']['building']
            except:
                pass

            try:
                address_description = item['address']['description']
            except:
                pass

            try:
                address_lat = item['address']['lat']
            except:
                pass

            try:
                address_lng = item['address']['lng']
            except:
                pass

            try:
                address_raw = item['address']['raw']
            except:
                pass

            try:
                address_metro = item['address']['metro']['station_name']
            except:
                pass

            try:
                address_id = item['address']['id']
            except:
                pass

        except:
            pass
            address_city = None
        
        try:
            employer_id = item['employer']['id']
        except:
            pass
            employer_id = None

        try:
            employer_url = item['employer']['url']
        except:
            pass
            employer_url = None
            

        try:
            employer_logo_url = item['employer']['logo_url']
        except:
            pass
            employer_logo_url = None

        
        try:
            employer_trusted = item['employer']['trusted']
        except:
            pass
            employer_trusted = True

        try:
            snippet_requirement = item['snippet']['requirement']
        except:
            pass
            snippet_requirement = None

        try:
            snippet_responsibility = item['snippet']['responsibility']
        except:
            pass
            snippet_responsibility = None

        try:
            contacts_name = item['contacts']['name']
        except:
            pass
            contacts_name = None

        try:
            contacts_email = item['contacts']['email']
        except:
            pass
            contacts_email = None

        try:
            contacts_phones = '{}-{}-{}'.format(item['contacts']['phones'][0]['country'], item['contacts']['phones'][0]['city'], item['contacts']['phones'][0]['number'])
        except:
            pass
            contacts_phones = None

        try:
            contacts_phones_comment = item['contacts']['phones'][0]['comment']
        except:
            pass
            contacts_phones_comment = None

        sql_query = """INSERT INTO items (item_id, premium, name, department, hast_test, reponse_letter_required, response_url, sort_point_distance, published_at, created_at, archived, insider_interview, apply_alternate_url, address_city, address_street, address_building, address_description, address_lat, address_lng, address_raw, address_metro, address_id, employer_id, employer_name, employer_url, employer_alternate_url, employer_vacancies_url, employer_trusted, snippet_requirement, snippet_responsibility, contacts_name, contacts_email, contacts_phones, contacts_phones_comment, sended_to_email, alternate_url) VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "{}", "False", "{}");""".format(item_id, premium, name, department, hast_test, reponse_letter_required, response_url, sort_point_distance, published_at, created_at, archived, insider_interview, apply_alternate_url, address_city, address_street, address_building, address_description, address_lat, address_lng, address_raw, address_metro, address_id, employer_id, employer_name, employer_url, employer_alternate_url, employer_vacancies_url, employer_trusted, snippet_requirement, snippet_responsibility, contacts_name, contacts_email, contacts_phones, contacts_phones_comment, alternate_url)

        try:
            #записываются только уникальные вакансии (определяется по UNIQUE url'у вакансии)
            cur.execute(sql_query)
            conn.commit()
            print('Success insert, page: {}'.format(page))
        except:
            pass
            print('NOT Success insert, page: {}'.format(page))

    cur.close()
    conn.close()

#Функция получения из основной таблицы записей (элементов) у которых нет номеров телефонов
def get_websites(db_name):
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    result = cur.execute("""SELECT item_id, employer_url FROM items WHERE contacts_phones = 'None' AND contacts_phones_comment = 'None'""")
    results = result.fetchall()

    for item in results:
        item_id = item[0]
        api_url = item[1]
        try:
            r = requests.get(api_url)
            data = parse_content(r.content.decode('utf-8'))
            if data['site_url'] != '':
                try:
                    cur.execute("""INSERT INTO items_website (item_id, website, parsed) VALUES ("{}", "{}", "False")""".format(item_id, data['site_url']))
                    conn.commit()
                    print('Row inserted, item_id: {}'.format(item_id))
                except:
                    pass
                    print('Row NOT inserted, item_id: {}'.format(item_id))
            else:
                pass
        except:
            pass

    cur.close()
    conn.close()

#функция обновления контактов (добавляется в комменты списком)
def update_contacts(db_name):
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    sql = cur.execute("""SELECT * FROM items_website WHERE parsed = 'False'""")
    sql_result = sql.fetchall()

    print('Count tasks', len(sql_result))
    for s in sql_result:
        item_id = s[1]
        url = s[2]

        #пробуем получить телефоны с сайтов
        try:
            phones = get_phone(url)
        except:
            pass
            phones = None

        #если телефон найден и кол-во больше нуля, то обновляем в основной таблице запись, если нет - пропускаем.
        if phones != None and len(phones) > 0:
            phones = ', '.join(phones)
            cur.execute("""UPDATE items SET contacts_phones_comment = '{}' WHERE item_id = '{}';""".format(phones, item_id))
            cur.execute("""UPDATE items_website SET parsed = 'True' WHERE item_id = '{}';""".format(item_id))
            conn.commit()
            print('Successfully updated row with item_id: {}'.format(item_id))

    cur.close()
    conn.close()


#Выводим последние за 24 часа элементы
def find_last_record_by_date(db_name, city):
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    delta_date = timedelta(1) #устанавливаем 24 часа "разницу во времени"
    today_date = datetime.now()

    #добавление от 19.12.2018 city - если указан, сделать запрос с городом, если нет - то делаем обычный запрос.
    if city != None:
        sql = cur.execute("""SELECT * FROM items WHERE sended_to_email = 'False' AND address_city = '{}'""".format(city)).fetchall()
    else:
        sql = cur.execute("""SELECT * FROM items WHERE sended_to_email = 'False'""").fetchall()
    
    #Циклом идем по каждой записи
    full_data = "<contacts>"
    for s in sql:
        record_date = datetime.strptime(s[9], "%Y-%m-%d %H:%M:%S") #конвертируем время из базы в читаемый модулем datetime формат


        if (today_date - record_date) <= delta_date:

            company_name = s[26]
            hr_name = s[34]
            hr_email = s[35]
            hr_phone = s[36]
            comment_phone = s[37]
            company_url = s[28]
            
            #добавление от 19.12.2018
            try:
                city = s[16]
            except:
                city = 'Не указан'

               
            if s[36] != '' and s[37] != '':
                full_hr_phone = '{}  {}'.format(none_replacer(hr_phone), none_replacer(comment_phone))
            else:
                full_hr_phone = ''
            #none_replacer(company_url)
            
            #создаем сообщение
            full_data += """
    <contact>
        <company>{}</company>
        <city>{}</city>
        <url>{}</url>
        <name>{}</name>
        <email>{}</email>
        <phone>{}</phone>
        <date>{}</date>
    </contact>""".format(none_replacer(company_name), city, none_replacer(company_url).replace('https://', ''), none_replacer(hr_name), none_replacer(hr_email), none_replacer(full_hr_phone), str(record_date))
            sql = ("""UPDATE items SET last_use = "{}", sended_to_email = "True" WHERE item_id = "{}" """.format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), s[1]))
            cur.execute(sql)
            conn.commit()
        else:
            pass
    full_data += '\n</contacts>'
    
    #посылаем через сендгрид
    
    return full_data

    cur.close()
    conn.close()

def check_ready_to_send(db_name):
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()

    today = datetime.now()
    sql = cur.execute("""SELECT item_id, last_use FROM items WHERE last_use is not NULL""").fetchall()
    
    for s in sql:
        last_use = s[1]
        #print(last_use)
        mes = datetime.strptime(last_use, "%Y-%m-%d %H:%M:%S")


        if (datetime.now() - mes) > timedelta(60):
            sql = cur.execute("""UPDATE items SET last_use = "{}", sended_to_email = "False" WHERE item_id = "{}" """.format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), s[0]))
            conn.commit()
            print("Updated last_use {}".format(s[0]))


def main():

    #имя базы данных sqlite3 и запрос к API
    db_name = 'sqlite3_db.db'
    #vacancy_name = "{}".format(namespace.query)
    vacancy_name = "специалист по охране труда"
    sendgrid_api_key = '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'
    sendgrid_from_email = 'bi@kioutlp.ru' #должен соответствовать тому, который указан в аккаунте
    sendgrid_to_email = 'maksimov@kiout.ru' #почта куда должны падать вакансии
    subject = 'Вакансии hh на дату: {}'.format(str(datetime.now())) #тема письма: <Вакансия hh на дату: 2018-12-12 08:30:55.334> (время запуска скрипта)
    api_url = 'https://api.hh.ru/vacancies' #задаем базовый URL API HH
    city = None #можно указать имя города, указывается так: 'Москва' или 'Самара'


    create_db(db_name) #создаем базу, если необходимо
    check_ready_to_send(db_name)

    page_content = get_content(api_url, vacancy_name) #отправляем запрос и получаем контент
    dict_content = parse_content(page_content) #превращаем контент страницы в словарь для обработки
    
    pages = int(dict_content['pages'])+1 #получаем кол-во страниц с вакансиями
    
    #Последовательно обходим каждую страницу из списка и сохраняем из нее данные в базу
    for page in range(0, pages):
        content = get_content(api_url, vacancy_name, page)
        dict_hh = parse_content(content)
        try:
            save_to_db(dict_hh['items'], db_name, page)
        except:
            pass

    #Берем все компании, у которых не указаны контакты (пустые строки в колонках contacts*) и сохраняем их адреса сайтов в отдельную таблицу.
    get_websites(db_name)

    #Из сохраненной таблицы (предыдущая функция), берем сайты и прогоняем через функцию поиска телефонов, сохраняем результаты
    #путем обновления исходных ячеек основной таблицы
    update_contacts(db_name)
    
    #выводим последние записи за 24 часа (timedeleta(1))
    fulldata = find_last_record_by_date(db_name, city)


    #в параметрах указывается отрправитель, адрес назначения, тема письма и тело письма. Формат письма: Raw.
    #выводится результат отправки (202) или False, если что-то не так.
    result_send = sendgrid_send(sendgrid_from_email, sendgrid_to_email, subject, fulldata, sendgrid_api_key)
    print(result_send)

if __name__ == '__main__':
    main()